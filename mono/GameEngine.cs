﻿using System;
using System.Collections.Generic;
using System.IO;
using HelloWorldOpen.Protocol;
using Newtonsoft.Json;

namespace HelloWorldOpen.Engine
{
    /// <summary>
    /// Class containing game related information
    /// </summary>
    public sealed class GameEngine
    {
        #region Attributes

        /// <summary>
        /// Specifies whether the game is running or not.
        /// </summary>
        private bool _gameRunning;

        private readonly TextWriter _writer;
        private readonly TextReader _reader;

        private RaceCar _myCar;
        private CarId _myId;
        private Driver _driver;
        private IList<RaceCar> _cars;
        private bool _logStop;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GameEngine" /> class.
        /// </summary>
        public GameEngine(Stream stream)
        {
            _reader = new StreamReader(stream);
            _writer = new StreamWriter(stream) {AutoFlush = true, NewLine = "\n" };
            _gameRunning = true;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        public string Name;

        /// <summary>
        /// Gets or sets the current tick of the game.
        /// </summary>
        public int Tick;

        #endregion

        #region Methods

        public void Join(SendMsg joinMsg)
        {
            _driver = new Driver();
            SendMsg(joinMsg);
        }

        public void Tournament()
        {  
            
            bool raceStarted = false;
            while (_gameRunning)
            {
                try
                {
                    string line = _reader.ReadLine();

                    if (line == null)
                    {
                        _gameRunning = false;
                        Log.Debug("<--> Received zero bytes from the remote host.");
                        continue;
                    }

                    if (!_logStop)
                    {
                        Log.Debug("<--- " + line);
                    }
                    SendMsg sendMsg;

                    MsgWrapperBase msg = JsonConvert.DeserializeObject<MsgWrapperBase>(line);

                    if (msg.msgType == "carPositions")
                    {
                        sendMsg = ManageThrottle(line, raceStarted);
                    }
                    else if (msg.msgType.IndexOf("error", StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        Log.Debug("ERROR: " + msg.msgType);
                        continue;
                    }
                    else
                    {
                        ReceiveMsgWrapper message = JsonConvert.DeserializeObject<ReceiveMsgWrapper>(line);

                        switch (msg.msgType)
                        {
                            case "turboAvailable":
                                Turbo turbo = message.GetData<Turbo>();
                                _driver.TurboAvailable(turbo);
                                sendMsg = new Ping(message.gameTick);
                                Log.Debug("TURBO AVAILABLE for " + turbo.turboDurationTicks + ", " + turbo.turboDurationMilliseconds + " ms, " + turbo.turboFactor);
                                break;

                            case "turboStart":
                                TurboNotification turboStart = message.GetData<TurboNotification>();
                                _driver.TurboStart(turboStart);
                                sendMsg = new Ping(message.gameTick);
                                break;

                            case "turboEnd":
                                TurboNotification turboEnd = message.GetData<TurboNotification>();
                                _driver.TurboEnd(turboEnd);
                                sendMsg = new Ping(message.gameTick);
                                break;

                            case "spawn":
                                Log.Debug("Spawned");
                                Spawn spawn = message.GetData<Spawn>();
                                bool spawned = _driver.Spawned(spawn);
                                if (spawned)
                                {
                                    _logStop = false;
                                }
                                sendMsg = new Ping(message.gameTick);
                                break;

                            case "crash":
                                Log.Debug(Environment.NewLine + "CRASHED!!!!" + Environment.NewLine);
                                sendMsg = new Ping(message.gameTick);
                                Crash crash = message.GetData<Crash>();
                                _logStop = _driver.Crashed(crash);
                                break;

                            case "yourCar":
                                _myId = message.GetData<CarId>();
                                Log.Debug("My car is " + _myId.color);
                                sendMsg = new Ping(message.gameTick);
                                break;

                            case "join":
                                Log.Debug("Joined the race");
                                sendMsg = new Ping(message.gameTick);
                                break;

                            case "gameInit":
                                Log.Debug("Race init");
                                GameInit race = message.GetData<GameInit>();

                                for (int i = 0; i < race.race.track.pieces.Count; ++i)
                                {
                                    race.race.track.pieces[i].Index = i;
                                }

                                raceStarted = false;
                                _logStop = false;
                                if (race.race.cars != null && race.race.cars.Count > 0)
                                {
                                    _cars = Helpers.GetRaceCars(race.race.cars);
                                    _myCar = Helpers.GetMyCar(_cars, _myId);
                                }
                                _driver.JoinRace(_myCar, race.race, _cars);
                                sendMsg = new Ping(message.gameTick);
                                break;

                            case "gameEnd":
                                raceStarted = false;
                                Log.Debug("Race ended");
                                sendMsg = new Ping(message.gameTick);
                                break;

                            case "gameStart":
                                raceStarted = true;
                                Log.Debug("Race starts");
                                sendMsg = new Ping(message.gameTick);
                                break;

                            case "tournamentEnd":
                                Log.Debug("Tournament end, did I won?");
                                _gameRunning = false;
                                raceStarted = false;
                                sendMsg = new Ping(message.gameTick);
                                break;

                            case "dnf":
                                Dnf dnf = message.GetData<Dnf>();
                                if (_driver.Dnf(dnf))
                                {
                                    raceStarted = false;
                                    _logStop = true;
                                }
                                sendMsg = new Ping(message.gameTick);
                                
                                Log.Debug("Race ended");
                                break;

                            default:
                                sendMsg = new Ping(message.gameTick);
                                break;
                        }
                    }

                    SendMsg(sendMsg);
                }

                catch (IOException e)
                {
                    Log.Debug(e.ToString());
                    _gameRunning = false;
                }
                catch (ObjectDisposedException e)
                {
                    Log.Debug(e.ToString());
                    _gameRunning = false;
                }
                catch (Exception ex)
                {
                    Log.Debug("Exception: " + ex);
                }
            }
        }

        private void SendMsg(SendMsg msg)
        {
            string line = msg.ToJson();
            if (!_logStop)
            {
                Log.Debug("---> " + line);
            }
            _writer.WriteLine(line);

        }

        private SendMsg ManageThrottle(string line, bool raceStarted)
        {
            CarPositions carPositions = JsonConvert.DeserializeObject<CarPositions>(line);
            Update(carPositions.data, carPositions.gameTick);
            
            return raceStarted ? _driver.Drive() : new Ping(carPositions.gameTick);
        }

        private void Update(IList<Position> positions, int gameTick)
        {
            foreach (RaceCar car in _cars)
            {
                Position position = Helpers.GetPosition(positions, car);
                car.Update(position, _driver.CurrentRace.track, gameTick);
            }
        }

        #endregion

    }
}
