﻿using System;
using System.Collections.Generic;
using HelloWorldOpen.Mathematics;
using HelloWorldOpen.Protocol;

namespace HelloWorldOpen.Engine
{
    public class Driver
    {
        private bool _first;

        private bool _crashed;

        private int _nextSwitchSendInd = -1;

        private double _previousAngle;

        private List<PieceGroup> _pieceGroups = new List<PieceGroup>();

        private PieceGroup _currentPieceGroup;

        private List<RaceCar> _cars;

        private bool _qualifying;

        private int _crashCount;

        private const int MaxCrashCount = 3;

        /// <summary>
        /// Gets or sets the car.
        /// </summary>
        private RaceCar MyCar;

        /// <summary>
        /// Gets or sets the track.
        /// </summary>
        public Race CurrentRace;

        /// <summary>
        /// Joins to the race.
        /// </summary>
        /// <param name="car">The car.</param>
        /// <param name="race">The race.</param>
        /// <param name="opponents">The opponents.</param>
        public void JoinRace(RaceCar car, Race race, IList<RaceCar> opponents)
        {
            _crashed = false;
            _first = true;
            _nextSwitchSendInd = -1;
            _previousAngle = 0;
            _cars = new List<RaceCar>();
            _currentPieceGroup = null;
            
            CurrentRace = race;
            foreach (RaceCar opponent in opponents)
            {
                if (opponent == car)
                {
                    continue;
                }
                _cars.Add(opponent);
            }

            MyCar = car;
            MyCar.Throttle = 1.0;
            
            if (_qualifying && race.raceSession.laps > 0 && _pieceGroups.Count > 0)
            {
                Log.Debug("RACE STARTS");
                _qualifying = false;
                return;
            }

            Log.Debug("QUALIFYING STARTS");
            
            _crashCount = 0;
            _qualifying = true;
            _pieceGroups = PieceGroup.Create(race.track.pieces, car.Properties.Length, CurrentRace.track.lanes);
        }

        /// <summary>
        /// Drives the car.
        /// </summary>
        /// <returns></returns>
        public SendMsg Drive()
        {
            if (_crashed)
            {
                return new Ping(MyCar.CurrentGameTick);
            }

            SwitchLane switchLane;
            if (_first || !TrySwitchLane(out switchLane))
            {
                _first = false;
                return Throttle(MyCar.CurrentGameTick);
            }
            return switchLane;
        }

        /// <summary>
        /// Tries to switch the current lane.
        /// </summary>
        /// <param name="switchLane">The switch lane.</param>
        /// <returns>
        /// True when swtich lane message can be sent.
        /// </returns>
        private bool TrySwitchLane(out SwitchLane switchLane)
        {
            switchLane = null;

            if (_nextSwitchSendInd < 0)
            {
                _nextSwitchSendInd = Helpers.PreviousOrLast(CurrentRace.track.GetNextSwitchPieceIndex(MyCar.CurrentTrackPiece.Index), CurrentRace.track.pieces.Count);
                Log.Debug("SWITCH NEXT AT " + _nextSwitchSendInd);
            }

            if (MyCar.CurrentTrackPiece.Index != _nextSwitchSendInd)
            {
                return false;
            }
            int index = Helpers.NextOrFirst(MyCar.CurrentTrackPiece.Index, CurrentRace.track.pieces.Count);
            Piece switchPiece = MyCar.NextSwitchPiece;
            bool overTaking;
            Direction dir = NextSwitchDirection(index, out overTaking);
            _nextSwitchSendInd = Helpers.PreviousOrLast(CurrentRace.track.GetNextSwitchPieceIndex(index), CurrentRace.track.pieces.Count);
            if (!overTaking && dir != Direction.None)
            {
                foreach (RaceCar car in _cars)
                {
                    if (!MyCar.CanSwitchLane(car, switchPiece.PreferredEndLane.distanceFromCenter))
                    {
                        Log.Debug("SHOULD NOT SWITCH NOW TO " + dir + " AT " + MyCar.CurrentTrackPiece.Index + ", NEXT AT " + _nextSwitchSendInd);
                        break;
                    }
                }
            }

            if (dir == Direction.None)
            {
                return false;
            }
            Log.Debug("SWITCH NOW TO " + dir + " AT " + MyCar.CurrentTrackPiece.Index + ", NEXT AT " + _nextSwitchSendInd);
            switchLane = new SwitchLane(dir, MyCar.CurrentGameTick);
            return true;
        }

        private Direction TryOvertake()
        {
            PieceGroup nextOrCurrentGroup = _pieceGroups[MyCar.NextSwitchPiece.PieceGroupIndex];
            
            if (nextOrCurrentGroup.Type != PieceGroupType.StraightLine && nextOrCurrentGroup.Type != PieceGroupType.Fast)
            {
                return Direction.None;
            }
            foreach (RaceCar car in _cars)
            {
                if (!car.IsHeadOf(MyCar, _pieceGroups))
                {
                    continue;
                }
                Direction dir;
                if (MyCar.NextSwitchPiece.IsCorner)
                {
                    dir = MyCar.NextSwitchPiece.GetDirection();
                }
                else
                {
                    if (!PieceGroup.IsFast(nextOrCurrentGroup.Type) || !MyCar.IsTurboAvailable)
                    {
                        if (!PieceGroup.IsFast(nextOrCurrentGroup.Type) || !MyCar.IsTurboAvailable)
                        {
                            return Direction.None;
                        }
                    }

                    dir = MyCar.NextSwitchPiece.PreferredEndLane.distanceFromCenter < MyCar.CurrentLane.distanceFromCenter ? Direction.Right : Direction.Left;
                }
                return dir;
            }
            return Direction.None;
        }

        /// <summary>
        /// Handles the throttle of the car.
        /// </summary>
        /// <param name="gameTick">The game tick.</param>
        /// <returns></returns>
        private SendMsg Throttle(int gameTick)
        {
            Piece piece = MyCar.CurrentTrackPiece;
            if (_currentPieceGroup == null || _currentPieceGroup.GroupId != piece.PieceGroupIndex)
            {
                SwitchGroup();
            }
            PieceGroup pieceGroup = _currentPieceGroup;

            if (MyCar.IsTurboMode)
            {
                pieceGroup.UpdateTurboStatus(true);
            }

            PieceGroup nextPieceGroup = _pieceGroups[Helpers.NextOrFirst(pieceGroup.GroupId, _pieceGroups.Count)];

            double speed = MyCar.Speed;
            double angle = MyCar.GetAngle();
            double friction = MyCar.Friction() + nextPieceGroup.BrakeForceAdjust;

            int distFromCenter = MyCar.CurrentLane.distanceFromCenter;

            double maxCornerSpeed = MaxSpeedForCorner(friction, nextPieceGroup.First.Index, true);
            const double brake = -0.15;
            double brakePoint = (int)MathHelper.CalculateBrakingPoint(speed, maxCornerSpeed, brake);

            if (MyCar.IsTurboMode)
            {
                pieceGroup.TryUpdateBrakePoint(brakePoint);
            }

            double extend = 0;
            if (IsCorner(nextPieceGroup.Type))
            {
                extend = nextPieceGroup.GetCornerExtension(NextCornerLane().distanceFromCenter);
            }

            double distanceToNextBend = (int)DistanceToNextCornerGroup(piece.Index, MyCar.CurrentInPiecePosition, pieceGroup.GroupId);
            distanceToNextBend += extend;

            double percentageCovered =
                    pieceGroup.GetPositionInGroup(piece.Index, MyCar.CurrentInPiecePosition, distFromCenter) /
                    pieceGroup.Distance(distFromCenter) * 100.0;

            UseTurbo turbo;
            if (distanceToNextBend <= brakePoint && 
                //nextPieceGroup.Type != PieceGroupType.StraightLine && 
               (nextPieceGroup.Type != PieceGroupType.Fast || (nextPieceGroup.Type == PieceGroupType.Fast && _currentPieceGroup.HadTurboInLap())))
            {
                double maxForNextGroup = MaxSpeedForCorner(friction + nextPieceGroup.BrakeForceAdjust, nextPieceGroup.First.Index, false);
                if (speed > maxForNextGroup)
                {
                    Log.Debug(MyCar.CurrentGameTick,
                        "BRAKE POINT CALC: " + brakePoint + " (" + nextPieceGroup.Type + "), dist: " + distanceToNextBend +
                        " (" + distanceToNextBend / brakePoint + "), speed: " + speed +
                        ", max for next: " + maxCornerSpeed + ", car accel: " + MyCar.MaxBreakForce + " (" + MyCar.Acceleration +
                        "), Angle: " + angle + ", " + percentageCovered + " %, full at " + _currentPieceGroup.FullThrottlePoint + " %");

                    MyCar.Throttle = 0.0;
                    _previousAngle = angle;
                    return new Throttle(MyCar.Throttle, gameTick);
                }
            }

            if (pieceGroup.Type != PieceGroupType.StraightLine)
            {
                if (nextPieceGroup.CanUseTurbo() && CanUseTurbo(percentageCovered, distanceToNextBend - extend, out turbo))
                {
                    _previousAngle = angle;
                    return turbo;
                }

                double maxBendSpeedCurrent = MaxSpeedForCorner(friction, piece.Index, false);
                MyCar.Throttle = Slide(speed, angle, maxBendSpeedCurrent, percentageCovered, nextPieceGroup);
            }
            else
            {
                if (pieceGroup.CanUseTurbo() && CanUseTurbo(percentageCovered, distanceToNextBend - extend, out turbo))
                {
                    _previousAngle = angle;
                    return turbo;
                }

                FullThrottle();
            }

            _previousAngle = angle;
            return new Throttle(MyCar.Throttle, gameTick);
        }

        private void SwitchGroup()
        {
            if (_currentPieceGroup != null)
            {
                double maxSpeed = _currentPieceGroup.GetMaxSpeed(false, MyCar.CurrentLane.index);
                double maxAngle = _currentPieceGroup.MaxAngle;
                if (MyCar.Properties.CurrentLap < 3 && !_currentPieceGroup.Crashed && _qualifying)
                {
                    maxSpeed = _currentPieceGroup.GetMaxSpeed(true, MyCar.CurrentLane.index);
                    if (maxAngle <= 38)
                    {
                        maxSpeed *= MyCar.Properties.CurrentLap < 2 ? 1.02 : 1.01;
                    }
                    else if (maxAngle <= 45)
                    {
                        maxSpeed *= MyCar.Properties.CurrentLap == 0 ? 1.01 : 1.005;
                    }
                    _currentPieceGroup.SetMaxSpeed(maxSpeed, MyCar.CurrentLane.index);
                }
                else if (maxSpeed <= 0)
                {
                    maxSpeed = _currentPieceGroup.GetMaxSpeed(false, MyCar.CurrentLane.index);
                    if (maxAngle <= 38)
                    {
                        maxSpeed *= MyCar.Properties.CurrentLap < 2 ? 1.01 : 1.005;
                    }
                    else if (maxAngle <= 45)
                    {
                        maxSpeed *= MyCar.Properties.CurrentLap == 1 ? 1.005 : 1.002;
                    }
                    
                    _currentPieceGroup.SetMaxSpeed(maxSpeed, MyCar.CurrentLane.index);
                }
                else if (_currentPieceGroup.MaxAngle >= 55)
                {
                    _currentPieceGroup.SetBrakeForce(0.01);
                    maxSpeed *= 0.98;
                    _currentPieceGroup.SetMaxSpeed(maxSpeed, MyCar.CurrentLane.index);
                }

                _currentPieceGroup.UpdateTurboStatus(false);
                _currentPieceGroup.TryUpdateBrakePoint(0);
            }
            _currentPieceGroup = _pieceGroups[MyCar.CurrentTrackPiece.PieceGroupIndex];
        }

        private double Slide(double speed, double angle, double maxSpeed, double groupPercentage, PieceGroup nextPieceGroup)
        {
            if (speed <= 0 || angle <= 0 || ((speed < maxSpeed && angle < 50) || MyCar.IsTurboMode))
            {
                FullThrottle();
                return MyCar.Throttle;
            }

            if (groupPercentage <= 15 && speed > maxSpeed * 1.1 && _currentPieceGroup.Type > PieceGroupType.Fast)
            {
                Log.Debug("FULL BRAKE WHILE SLIDING, " + angle + ", extension set " + _currentPieceGroup.GetCornerExtension(NextCornerLane().distanceFromCenter));
                return 0.0;
            }

            if (angle >= 50)
            {
                _currentPieceGroup.Crashed = true;
            }

            _currentPieceGroup.MaxAngle = angle;

            double throttle;
            double angleDiff = (_previousAngle / angle);

            if (angleDiff > 0.95)
            {
                if (angle < 45 && ((groupPercentage >= _currentPieceGroup.FullThrottlePoint && !PieceGroup.IsSlowCorner(nextPieceGroup.Type)) ||
                    (speed < maxSpeed && _currentPieceGroup.Type <= PieceGroupType.Fast)))
                {
                    FullThrottle();
                    Log.Debug(MyCar.CurrentGameTick, "FULL AT THE BEND... " + MyCar.Throttle + ", max: " + maxSpeed + ", " + groupPercentage + ", full at " + _currentPieceGroup.FullThrottlePoint + " %");
                    return MyCar.Throttle;
                }
            }
            if (angleDiff < 0.99)
            {
                if (!_currentPieceGroup.Crashed && angle < 45)
                {
                    _currentPieceGroup.SetTempMaxSpeed(speed);
                }

                switch (_currentPieceGroup.Type)
                {
                    case PieceGroupType.Fast:
                        throttle = angle > 20 && speed > maxSpeed * 1.2 ? Math.Pow(angleDiff, 2) : 1.0;
                        break;
                    case PieceGroupType.Normal:
                        throttle = Math.Min(Math.Max(Math.Pow(1 - (angle / 90), 4) * Math.Pow(maxSpeed / speed, 3), 0.001), 1.0);
                        break;
                    case PieceGroupType.LTurn:
                        throttle = Math.Min(Math.Max((1 - (angle / 90)) * Math.Pow(maxSpeed / speed, 3) * Math.Pow(angleDiff, 15), 0.001), 1.0);
                        break;
                    default:
                        throttle = Math.Min(Math.Max((1 - (angle / 90)) * Math.Pow(maxSpeed / speed, 3) * Math.Pow(angleDiff, 20), 0.0), 1.0);
                        break;
                }
                Log.Debug(MyCar.CurrentGameTick, "ANGULAR THROTTLE (INC): " + _currentPieceGroup.Type + ", " + MyCar.Throttle + " --> " + throttle + ", angle: " + angle + ", prev angle: " + _previousAngle + ", MAXSPEED: " + maxSpeed + ", SPEED: " + speed + ", ACCEL: " + MyCar.Friction() + ", " + groupPercentage + " %, full at " + _currentPieceGroup.FullThrottlePoint + " %");
            }
            else
            {
                if (_currentPieceGroup.Type > PieceGroupType.Fast && nextPieceGroup.Type != PieceGroupType.StraightLine)
                {
                    int idx = MyCar.CurrentTrackPiece.PieceGroupIndex - 1;
                    Direction previous = _pieceGroups[idx < 0 ? _pieceGroups.Count - 1 : idx].Direction;
                    Direction current = MyCar.CurrentTrackPiece.GetDirection();

                    if (current != previous)
                    {
                        throttle = Math.Min(Math.Max(Math.Pow(maxSpeed / speed, _currentPieceGroup.Type == PieceGroupType.Normal ? 4 : 20), 0.0), 1.0);
                        Log.Debug(MyCar.CurrentGameTick, "ANGULAR THROTTLE (DEC ~): " + _currentPieceGroup.Type + ", " + previous + " --> " + current + ", " + MyCar.Throttle + " --> " + throttle + " , angle: " + angle + ", prev angle: " + _previousAngle + ", MAXSPEED: " + maxSpeed + ", SPEED: " + speed + ", ACCEL: " + MyCar.Friction() + ", " + groupPercentage + " %, full at " + _currentPieceGroup.FullThrottlePoint + " %");
                        return throttle;
                    }
                }

                if (angle > 30 && groupPercentage < 20 || _currentPieceGroup.Type == PieceGroupType.UTurn || _currentPieceGroup.Type == PieceGroupType.LTurn)
                {
                    throttle = Math.Min(Math.Max(Math.Pow(1 - (angle / 135), 3), 0), 1.0);
                }
                else
                {
                    throttle = 1;
                }
                Log.Debug(MyCar.CurrentGameTick, "ANGULAR THROTTLE (DEC): " + _currentPieceGroup.Type + ", " + MyCar.Throttle + " --> " + throttle + ", angle: " + angle + ", prev angle: " + _previousAngle + ", MAXSPEED: " + maxSpeed + ", SPEED: " + speed + ", ACCEL: " + MyCar.Friction() + ", " + groupPercentage + " %, full at " + _currentPieceGroup.FullThrottlePoint + " %");
            }

            return throttle;
        }

        private void FullThrottle()
        {
            MyCar.Throttle = 1.0;
        }

        private double DistanceToNextCornerGroup(int index, double piecePosition, int groupIndex)
        {
            int distanceFromCenter = NextCornerLane().distanceFromCenter;
            PieceGroup pg = _pieceGroups[groupIndex];
            double distance = pg.Distance(distanceFromCenter) - pg.GetPositionInGroup(index, piecePosition, distanceFromCenter);
            while (true)
            {
                PieceGroup next = _pieceGroups[Helpers.NextOrFirst(groupIndex, _pieceGroups.Count)];
                groupIndex = next.GroupId;

                if (pg.GroupId == groupIndex || next.Type != PieceGroupType.StraightLine)
                {
                    return distance;
                }
                distance += next.Distance(distanceFromCenter);
            }

        }

        private bool CanUseTurbo(double groupPercentage, double distance, out UseTurbo turbo)
        {
            turbo = null;

            if (MyCar.IsTurboAvailable &&
               (_currentPieceGroup.Type == PieceGroupType.StraightLine || groupPercentage >= 90 ||
               (_currentPieceGroup.Type == PieceGroupType.Fast && groupPercentage > 75 && MyCar.GetAngle() < 50)))
            {
                double turboDist = MyCar.Speed * MyCar.Turbo.turboDurationTicks * Math.Sqrt(MyCar.Turbo.turboFactor);
                if (distance > turboDist)
                {
                    Log.Debug(MyCar.CurrentGameTick,
                        "USING TURBO: distance to bend: " + (distance) +
                        ", car distance with turbo " + turboDist);
                    turbo = new UseTurbo(".... 6 .. 5 .. 4 .. 3 .. 2 .. 1 .. 0 ... We have a liftoff ....", MyCar.CurrentGameTick);
                    return true;
                }
            }
            return false;
        }

        private static bool IsCorner(PieceGroupType type)
        {
            return type != PieceGroupType.StraightLine;
        }

        private Direction NextSwitchDirection(int index, out bool overtaking)
        {
            overtaking = false;

            MyCar.NextSwitchPiece = CurrentRace.track.pieces[index];
            LaneDetail endLane = MyCar.NextSwitchPiece.PreferredEndLane;
            Direction overtake = TryOvertake();

            if (overtake != Direction.None)
            {
                overtaking = true;
                return overtake;
            }

            if (endLane.distanceFromCenter == MyCar.CurrentLane.distanceFromCenter)
            {
                return Direction.None;
            }

            return endLane.distanceFromCenter < MyCar.CurrentLane.distanceFromCenter ? Direction.Left : Direction.Right;
        }

        private double MaxSpeedForCorner(double acceleration, int index, bool nextCorner)
        {
            Piece piece = CurrentRace.track.GetNextBend(index, true);
            PieceGroup pg = _pieceGroups[piece.PieceGroupIndex];
            LaneDetail lane = nextCorner ? NextCornerLane() : MyCar.CurrentLane;
            double max = pg.GetMaxSpeed(false, lane.index);
            double speed = max > 0 ? max : piece.MaxSpeed(lane.distanceFromCenter, MyCar.Properties.Length, MyCar.Properties.GuidanceFlagPosition, acceleration);
            return speed;
        }

        private LaneDetail NextCornerLane()
        {
            if (MyCar.CurrentTrackPiece.HasSwitch)
            {
                return MyCar.CurrentLane;
            }
            bool theSame = MyCar.NextSwitchPiece.PieceGroupIndex == MyCar.CurrentTrackPiece.PieceGroupIndex;
            return theSame ? MyCar.CurrentLane : MyCar.NextSwitchPiece.PreferredEndLane;
        }

        public void TurboAvailable(Turbo turbo)
        {
            if (_crashed)
            {
                return;
            }
            MyCar.TurboAvailable(turbo);
        }

        public void TurboStart(TurboNotification turboStart)
        {
            if (turboStart.name == MyCar.Properties.Name)
            {
                MyCar.UseTurbo();
            }
        }

        public void TurboEnd(TurboNotification turboEnd)
        {
            if (turboEnd.name == MyCar.Properties.Name)
            {
                MyCar.TurboEnded();
            }
        }

        public bool Dnf(Dnf dnf)
        {
            if (dnf.car.name != MyCar.Properties.Name)
            {
                return false;
            }
            _crashed = true;
            return true;
        }

        public bool Spawned(Spawn spawn)
        {
            if (spawn.name == MyCar.Properties.Name)
            {
                _crashed = false;
                return true;
            }
            return false;
        }

        public bool Crashed(Crash message)
        {
            if (message.name == MyCar.Properties.Name)
            {
                _crashed = true;
                MyCar.ClearTurbo();

                if (++_crashCount >= MaxCrashCount) // TODO
                {
                    foreach (PieceGroup pg in _pieceGroups)
                    {
                        pg.SetBrakeForce(0.005);
                        pg.Crashed = true;
                    }
                }

                PieceGroup pg2 = _currentPieceGroup;
                if (!IsCorner(_currentPieceGroup.Type))
                {
                    pg2 = _pieceGroups[Helpers.PreviousOrLast(_currentPieceGroup.GroupId, _pieceGroups.Count)];
                }

                pg2.SetCornerExtension(-0.2);
                pg2.SetBrakeForce(0.02);
                pg2.SetMaxSpeed(pg2.GetMaxSpeed(false, MyCar.CurrentLane.index) * 0.95, MyCar.CurrentLane.index);
                pg2.Crashed = true;
                return true;
            }
            return false;
        }

        
    }
}
