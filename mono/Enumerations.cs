﻿namespace HelloWorldOpen.Engine
{
    /// <summary>
    /// Enumerates the directions.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// No direction.
        /// </summary>
        None = 0,
        /// <summary>
        /// Direction to the left.
        /// </summary>
        Left = -1,
        /// <summary>
        /// Direction to the right.
        /// </summary>
        Right = 1
    }
}
