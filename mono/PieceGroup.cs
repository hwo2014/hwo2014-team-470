﻿using System;
using System.Collections.Generic;
using HelloWorldOpen.Protocol;

namespace HelloWorldOpen.Engine
{
    public enum PieceGroupType
    {
        StraightLine = 0,
        Fast = 1,
        Normal = 2,
        LTurn = 3,
        UTurn = 4
    }

    public class PieceGroup
    {
        private double _fullThrottlePoint = 75;

        public PieceGroupType Type;

        public int GroupId;

        public double Angle;

        public double Radius = int.MaxValue;

        public Direction Direction = Direction.None;

        private readonly List<Piece> _pieces = new List<Piece>();

        private bool _turboInUse;

        private double _brakePoint;

        private bool _canUseTurbo;

        private double _totalAngle;

        private double _extensionMultiplier;

        private readonly double[] _maxSpeeds;

        private double _maxSpeedTemp;

        private double _maxAngle;

        private double _brakeForce;

        private bool _crashed;

        private PieceGroup(int lanes)
        {
            _maxSpeeds = new double[lanes];
        }

        public void Add(Piece piece)
        {
            if (!_pieces.Contains(piece))
            {
                piece.PieceGroupIndex = GroupId;
                _pieces.Add(piece);
            }
        }

        /// <summary>
        /// Returns distance of the group (combined by all pieces).
        /// </summary>
        /// <param name="distanceFromCenter">The distance from center.</param>
        /// <returns></returns>
        public double Distance(int distanceFromCenter)
        {
            double distance = 0;
            foreach (Piece piece in _pieces)
            {
                distance += piece.GetLength(distanceFromCenter);
            }
            return distance;
        }

        /// <summary>
        /// Gets the position in the group using single lane (close enough for our purpose)
        /// </summary>
        /// <param name="pieceIndex">Index of the piece.</param>
        /// <param name="inPiecePosition">The in piece position.</param>
        /// <param name="distanceFromCenter">The distance from center.</param>
        /// <returns></returns>
        public double GetPositionInGroup(int pieceIndex, double inPiecePosition, int distanceFromCenter)
        {
            double distance = 0;
            foreach (Piece piece in _pieces)
            {
                if (piece.Index == pieceIndex)
                {
                    distance += inPiecePosition;
                    break;
                }
                distance += piece.GetLength(distanceFromCenter);
            }
            return distance;
        }

        public Piece First
        {
            get { return _pieces[0]; }
        }

        public Piece Last
        {
            get { return _pieces[_pieces.Count - 1]; }
        }

        public bool HasStraight
        {
            get
            {
                foreach (Piece piece in _pieces)
                {
                    if (!piece.IsCorner)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public double TotalAngle
        {
            get { return _totalAngle; }
        }

        public int Count
        {
            get { return _pieces.Count; }
        }

        public double FullThrottlePoint
        {
            get { return _fullThrottlePoint; }
        }

        public bool CanUseTurbo()
        {
            return _canUseTurbo && _pieces.Count > 3 && Type == PieceGroupType.StraightLine;
        }

        public void UpdateTurboStatus(bool turboHasBeenUsed)
        {
            _turboInUse = turboHasBeenUsed;
        }

        public bool HadTurboInLap()
        {
            return _turboInUse;
        }

        public void SetTempMaxSpeed(double value)
        {
            _maxSpeedTemp = value;
        }

        public void SetMaxSpeed(double value, int laneIndex)
        {
            _maxSpeeds[laneIndex] = value;
            _maxSpeedTemp = 0;
        }

        public double BrakeForceAdjust
        {
            get { return _brakeForce; }
        }

        public bool Crashed
        {
            get { return _crashed; }
            set { _crashed = value; }
        }

        public double MaxAngle
        {
            get { return _maxAngle; }
            set
            {
                if (value > _maxAngle)
                {
                    _maxAngle = value;
                }
            }
        }

        public void Setup(PieceGroup previous, PieceGroup next, double carLength, double maxRadius, bool canUseTurbo)
        {
            _canUseTurbo = canUseTurbo;
            if (Type != PieceGroupType.StraightLine)
            {
                Direction = First.GetDirection();
                Angle = Math.Abs(First.angle.Value);

                bool hasMinRadius = false;
                foreach (Piece piece in _pieces)
                {
                    if (piece.radius.HasValue)
                    {
                        if (piece.radius.Value < Radius)
                        {
                            Radius = piece.radius.Value;
                        }
                        if (piece.radius.Value < carLength * 1.5)
                        {
                            hasMinRadius = true;
                        }
                    }

                    _totalAngle += piece.angle.HasValue ? Math.Abs(piece.angle.Value) : 0;
                }

                if (hasMinRadius && _totalAngle >= 135)
                {
                    Type = PieceGroupType.UTurn;
                }
                else if (hasMinRadius)
                {
                    Type = PieceGroupType.LTurn;
                }
                else if (Count == 1 && Math.Abs(Angle) < 45 && 
                         previous.Type == PieceGroupType.StraightLine && 
                         next.Type == PieceGroupType.StraightLine)
                {
                    Type = PieceGroupType.Fast; // TODO: merge straight lines into one group
                }
                else if (Math.Abs(Angle) < 45 && Radius >= maxRadius &&
                         previous.Type == PieceGroupType.StraightLine &&
                         (next.Type == PieceGroupType.StraightLine || next.Type == PieceGroupType.Normal))
                {
                    Type = PieceGroupType.Fast;
                }
                else
                {
                    Type = PieceGroupType.Normal;
                    _extensionMultiplier = 0.5;
                }

                if (next.Type == PieceGroupType.UTurn || next.Type == PieceGroupType.LTurn)
                {
                    _fullThrottlePoint = 80;
                }
                else if (HasStraight || Type == PieceGroupType.UTurn || Type == PieceGroupType.LTurn)
                {
                    _fullThrottlePoint = 80;
                }
                else
                {
                    _fullThrottlePoint = 60;
                }
            }
        }

        public double TryUpdateBrakePoint(double brakePoint)
        {
            if (brakePoint == 0 || brakePoint * 1.1 > _brakePoint)
            {
                _brakePoint = brakePoint * 1.1;
            }
            return _brakePoint;
        }

        public override string ToString()
        {
            return GroupId + ", " + Type + ", " + _pieces.Count + ", " + Radius + ", " + Angle + ", " + Direction + ", " + Distance(0) + ", " + _fullThrottlePoint + " %";
        }

        public static List<PieceGroup> Create(List<Piece> list, double carLength, List<LaneDetail> lanes)
        {
            List<PieceGroup> pieceGroups = new List<PieceGroup>();
            LinkedList<Piece> pieces = new LinkedList<Piece>(list);
            LinkedListNode<Piece> first = pieces.First;

            while (first.Value.IsCorner)
            {
                first = first.Previous ?? pieces.Last;
                if (first == pieces.First)
                {
                    break;
                }
            }

            LinkedListNode<Piece> startPoint = first.Previous ?? pieces.Last;
            if (!first.Value.IsCorner)
            {

                while (startPoint != first)
                {
                    if (startPoint.Value.IsCorner)
                    {
                        break;
                    }
                    first = startPoint;
                    startPoint = startPoint.Previous;
                }
            }

            PieceGroup group = null;
            while (true)
            {
                Piece current = first.Value;
                if (current.IsCorner)
                {
                    if (group == null || group.Type == PieceGroupType.StraightLine || group.First.GetDirection() != current.GetDirection() ||
                        group.First.radius != current.radius || Math.Abs(current.angle.Value) > (group.First.angle.HasValue ? Math.Abs(group.First.angle.Value) : -1))
                    {
                        int groupId = group != null ? group.GroupId + 1 : 0;
                        group = new PieceGroup(lanes.Count);
                        group.GroupId = groupId;
                        group.Type = PieceGroupType.Normal;
                        pieceGroups.Add(group);
                    }
                    group.Add(current);
                }
                else
                {
                    if (group == null || (group.Type == PieceGroupType.Normal))
                    {
                        int groupId = group != null ? group.GroupId + 1 : 0;
                        group = new PieceGroup(lanes.Count);
                        group.GroupId = groupId;
                        group.Type = PieceGroupType.StraightLine;
                        pieceGroups.Add(group);
                    }
                    group.Add(current);
                }

                if (first == startPoint)
                {
                    break;
                }

                first = first.Next ?? pieces.First;
            }

            double maxRadius = 0;
            foreach (Piece piece in pieces)
            {
                if (piece.radius.HasValue)
                {
                    if (piece.radius.Value > maxRadius)
                    {
                        maxRadius = piece.radius.Value;
                    }
                }
            }
            PieceGroup previous = pieceGroups[pieceGroups.Count - 1];
            PieceGroup next = pieceGroups[pieceGroups.Count > 0 ? 1 : 0];
            PieceGroup gr = pieceGroups[0];
            gr.Setup(previous, next, carLength, maxRadius, gr.Type == PieceGroupType.StraightLine);
            for (int i = pieceGroups.Count - 1; i >= 0; --i)
            {
                next = gr;
                previous = pieceGroups[Helpers.PreviousOrLast(i, pieceGroups.Count)];
                gr = pieceGroups[i];
                gr.Setup(previous, next, carLength, maxRadius, gr.Type == PieceGroupType.StraightLine);
            }

            // merge single pieces into straight lines
            for (int i = 1; i < pieceGroups.Count - 1;)
            {
                previous = pieceGroups[i - 1];
                next = pieceGroups[i + 1];
                PieceGroup current = pieceGroups[i];

                // TODO: älä yhdistä jos on kaksi lturnia peräkkäin
                if (current.Count == 1 && current.Type != PieceGroupType.Fast &&
                    previous.Type == PieceGroupType.StraightLine && 
                    (next.Type == PieceGroupType.StraightLine || (next.Count == 1 && next.Type == PieceGroupType.LTurn)) || 
                    (previous.Type == PieceGroupType.StraightLine && current.Type == PieceGroupType.StraightLine))
                {
                    
                    previous.Add(current.First);
                    foreach (Piece piece in next._pieces)
                    {
                        previous.Add(piece);
                    }
                    pieceGroups.RemoveAt(i); // current
                    pieceGroups.RemoveAt(i); // next

                    for (int j = i; j < pieceGroups.Count; ++j)
                    {
                        pieceGroups[j].GroupId = j;
                        foreach (Piece p in pieceGroups[j]._pieces)
                        {
                            p.PieceGroupIndex = j;
                        }
                    }
                    // skip next 
                }
                ++i;
            }

            foreach (Piece p in list)
            {
                if (!p.HasSwitch)
                {
                    continue;
                }

                double shortest = double.MaxValue;
                LaneDetail shortestLane = null;
                
                foreach (LaneDetail lane in lanes)
                {
                    int index = p.Index;
                    double len = 0;
                    while (true)
                    {
                        index = Helpers.NextOrFirst(index, list.Count);
                        Piece nextPiece = list[index];
                        if (p.Index == nextPiece.Index || nextPiece.HasSwitch)
                        {
                            break;
                        }

                        len += nextPiece.GetLength(lane.distanceFromCenter);
                    }

                    if (len < shortest)
                    {
                        shortest = len;
                        shortestLane = lane;
                    }
                    else if (len == shortest)
                    {
                        shortestLane = null;
                    }
                }

                p.PreferredEndLane = shortestLane;

            }

            for (int i = 0; i < list.Count; ++i)
            {
                if (list[i].PreferredEndLane == null)
                {
                    int index = Helpers.NextOrFirst(i, list.Count);
                    while (!list[index].HasSwitch)
                    {
                        index = Helpers.NextOrFirst(index, list.Count);
                    }
                    list[i].PreferredEndLane = list[index].PreferredEndLane;
                }
            }

            return pieceGroups;
        }

        public static bool IsSlowCorner(PieceGroupType type)
        {
            return type == PieceGroupType.LTurn || type == PieceGroupType.UTurn;
        }

        public static bool IsFast(PieceGroupType type)
        {
            return type == PieceGroupType.StraightLine || type == PieceGroupType.Fast;
        }

        public void SetCornerExtension(double value)
        {
            if (value < 0 || Type <= PieceGroupType.LTurn)
            {
                _extensionMultiplier += value;
            }
        }

        public double GetCornerExtension(int distanceFromCenter)
        {
            if (Type == PieceGroupType.Fast)
            {
                return 0;
            }
            return First.GetLength(distanceFromCenter) * _extensionMultiplier;
        }

        public double GetMaxSpeed(bool temp, int laneIndex)
        {
            return temp ? _maxSpeedTemp : _maxSpeeds[laneIndex];
        }

        public void SetBrakeForce(double force)
        {
            _brakeForce += force;
        }
    }
}
