﻿using System;
using System.IO;

namespace HelloWorldOpen
{
    public class Log
    {
        //private const string Filename = "hwo_debug.log";

        //private static readonly StreamWriter _writer;
        //private static readonly FileStream _fileStream;

        static Log()
        {
            //_fileStream = File.Open(Filename, FileMode.Append, FileAccess.Write, FileShare.Delete | FileShare.ReadWrite);
            //_writer = new StreamWriter(_fileStream);
            //_writer.AutoFlush = true;
            // file handle closed when app closed by framework / os
        }

        /// <summary>
        /// Writes the <paramref name="message"/> to debug log and includes the <paramref name="tick"/> into the complete message.
        /// </summary>
        /// <param name="tick">The tick.</param>
        /// <param name="message">The message.</param>
        public static void Debug(int tick, string message)
        {
            string line = String.Format("{0}: {1}: {2}", DateTime.Now, tick, message);
            WriteLine(line);
        }

        /// <summary>
        /// Writes the <paramref name="message"/> to debug log.
        /// </summary>
        /// <param name="message">The message.</param>
        public static void Debug(string message)
        {
            string line = String.Format("{0}: {1}", DateTime.Now, message);
            WriteLine(line);
        }

        private static void WriteLine(string line)
        {
			Console.WriteLine(line);
/*
            lock (_writer)
            {
                try
                {
                    _writer.WriteLine(line);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
*/
        }
    }
}
