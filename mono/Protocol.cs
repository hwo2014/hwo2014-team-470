﻿using System;
using System.Collections.Generic;
using HelloWorldOpen.Engine;
using HelloWorldOpen.Mathematics;
using Newtonsoft.Json;

namespace HelloWorldOpen.Protocol
{
    public class MsgWrapperBase
    {
        public string msgType;
    }

    public class MsgWrapper : MsgWrapperBase
    {
        public Object data;

        public int gameTick;

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }

        public MsgWrapper(string msgType, Object data, int gameTick)
            : this (msgType, data)
        {
            this.gameTick = gameTick;
        }
    }

    public abstract class SendMsg
    {
        public int gameTick;

        protected SendMsg(int gameTick)
        {
            this.gameTick = gameTick;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData(), this.GameTick()));
        }

        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();

        protected int GameTick()
        {
            return gameTick;
        }
    }

    public class ReceiveMsgWrapper : MsgWrapperBase
    {
        public object data;
        public string gameId;
        public int gameTick;

        public ReceiveMsgWrapper(string msgType, object data)
        {
            this.msgType = msgType;
            this.data = data;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public T GetData<T>()
        {
            string d = data.ToString();
            return JsonConvert.DeserializeObject<T>(d);
        }
    }

    public abstract class ReceiveMsg
    {
    }

    public class Join : SendMsg
    {
        public string name;
        public string key;

        public Join(string name, string key, int gameTick)
            : base(gameTick)
        {
            this.name = name;
            this.key = key;
        }

        protected override string MsgType()
        {
            return "join";
        }
    }

    public class Ping : SendMsg
    {

        public Ping(int gameTick)
            : base(gameTick)
        {
        }

        protected override string MsgType()
        {
            return "ping";
        }
    }

    public class Throttle : SendMsg
    {
        public double value;

        public Throttle(double value, int gameTick)
            : base(gameTick)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }

    public class SwitchLane : SendMsg
    {
        public string value;

        public SwitchLane(Direction value, int gameTick)
            : base(gameTick)
        {
            this.value = value.ToString();
            this.gameTick = gameTick;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }

    public class CarId
    {
        protected bool Equals(CarId other)
        {
            return string.Equals(name, other.name) && string.Equals(color, other.color);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((name != null ? name.GetHashCode() : 0) * 397) ^ (color != null ? color.GetHashCode() : 0);
            }
        }

        public string name;
        public string color;

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((CarId) obj);
        }

        public override string ToString()
        {
            return name + ", " + color;
        }
    }

    public class Lane
    {
        public int startLaneIndex;
        public int endLaneIndex;
    }

    public class PiecePosition
    {
        public int pieceIndex;
        public double inPieceDistance;
        public Lane lane;
        public int lap;

        public bool IsSwitching()
        {
            return lane.startLaneIndex != lane.endLaneIndex;
        }
    }

    public class Position
    {
        public CarId id;
        public double angle;
        public PiecePosition piecePosition;
    }

    public class CarPositions : MsgWrapperBase
    {
        public List<Position> data;
        public string gameId;
        public int gameTick;
    }


    public class Piece
    {
        public double length;
        public bool? @switch;
        public int? radius;
        public double? angle;
        internal int Index;
        internal int PieceGroupIndex;
        internal LaneDetail PreferredEndLane = new LaneDetail();

        public double GetLength(double laneDistanceFromCenterTrack, double laneDiff, bool switchedPiece)
        {
            switch (GetDirection())
            {
                case Direction.None:
                    return length;
                case Direction.Right:
                    return MathHelper.CalculateArcLength(radius.Value, angle.Value, -laneDistanceFromCenterTrack);
                case Direction.Left:
                    return MathHelper.CalculateArcLength(radius.Value, angle.Value, laneDistanceFromCenterTrack);
            }

            throw new NotSupportedException("The current piece is so complicated that I do not understand it.");
        }

        public double GetLength(double laneDistanceFromCenterTrack)
        {
            return GetLength(laneDistanceFromCenterTrack, 0, false);
        }

        public double GetRemainingLength(double inPiecePosition, double laneDistanceFromCenterTrack)
        {
            return GetLength(laneDistanceFromCenterTrack) - inPiecePosition;
        }

        public Direction GetDirection()
        {
            if (!angle.HasValue || Math.Abs(angle.Value) <= 0.0)
            {
                return Direction.None;
            }
            return angle.Value < 0.0 ? Direction.Left : Direction.Right;
        }

        public bool HasSwitch
        {
            get { return @switch.HasValue && @switch.Value; }
        }

        public bool IsCorner
        {
            get { return angle.HasValue && Math.Abs(angle.Value) > 0.0; }
        }

        public double MaxSpeed(int laneDistanceFromCenterTrack, double carLength, double guideFlag, double acceleration)
        {
            if (!radius.HasValue || !angle.HasValue)
            {
                return double.MaxValue;
            }
            double r = GetRadius(laneDistanceFromCenterTrack, carLength, guideFlag);
            double a = MathHelper.GetAngularAcceleration(r, angle.Value, acceleration);
            return a;
        }

        public double GetRadius(int distanceFromCenter, double carLength, double guideFlag)
        {
            if (!radius.HasValue)
            {
                return 0;
            }

            double r = radius.Value;
            switch (GetDirection())
            {
                case Direction.Right:
                    r = radius.Value - distanceFromCenter;
                    break;
                case Direction.Left:
                    r = radius.Value + distanceFromCenter;
                    break;
            }
            double c = carLength - guideFlag;
            return r + (0.1 * c);
        }

        public override string ToString()
        {
            return Index + ", " + PieceGroupIndex + ", " + GetLength(0) + ", " + GetDirection() + ", " + angle + ", " + radius + ", Switch: " + HasSwitch;
        }
    }

    public class LaneDetail
    {
        public int distanceFromCenter;
        public int index;
    }

    public class Track
    {
        public string id;
        public string name;
        public List<Piece> pieces;
        public List<LaneDetail> lanes;

        public Piece GetNextPiece(int pieceIndex)
        {
            return pieces[Helpers.NextOrFirst(pieceIndex, pieces.Count)];
        }

        public Piece GetNextBend(int currentIndex, bool includeCurrentPiece)
        {
            if (includeCurrentPiece && pieces[currentIndex].IsCorner)
            {
                return pieces[currentIndex];
            }

            int index = Helpers.NextOrFirst(currentIndex, pieces.Count);
            while (index != currentIndex)
            {
                if (pieces[index].IsCorner)
                {
                    return pieces[index];
                }
                index = Helpers.NextOrFirst(index, pieces.Count);
            }
            return null;
        }

        public int GetNextSwitchPieceIndex(int currentPieceIndex)
        {
            int index = Helpers.NextOrFirst(currentPieceIndex, pieces.Count);
            while (index != currentPieceIndex)
            {
                if (pieces[index].HasSwitch)
                {
                    return index;
                }
                index = Helpers.NextOrFirst(index, pieces.Count);
            }
            return -1;
        }

        public Piece GetPreviousPiece(int currentIndex)
        {
            return pieces[--currentIndex < 0 ? pieces.Count - 1 : currentIndex];
        }

        public double DistanceToNextCorner(int distanceFromCenter, int currentIndex, double inPieceDistance)
        {
            Piece current = pieces[currentIndex];
            double distance = current.GetLength(distanceFromCenter) - inPieceDistance;
            int index = Helpers.NextOrFirst(currentIndex, pieces.Count);
            while (index != currentIndex)
            {
                if (pieces[index].IsCorner)
                {
                    return distance;
                }
                distance += pieces[index].GetLength(distanceFromCenter);
                index = Helpers.NextOrFirst(index, pieces.Count);
            }
            return double.MaxValue;
        }

        public double DistanceToRaceEnd(int laps, int currentPiece, int currentLap)
        {
            if (currentLap != laps - 1)
            {
                return double.MaxValue;
            }

            Piece current = pieces[currentPiece];
            Piece next = GetNextPiece(currentPiece);
            double distance = current.GetLength(0);
            while (current.Index < next.Index)
            {
                distance += next.GetLength(0);
                next = GetNextPiece(next.Index);
            }
            return distance;
        }
    }

    public class Dimensions
    {
        public double length;
        public double width;
        public double guideFlagPosition;
    }

    public class Car
    {
        public CarId id;
        public Dimensions dimensions;
    }

    public class RaceSession
    {
        public int laps;
        public int maxLapTimeMs;
        public bool quickRace;
    }

    public class Race
    {
        public Track track;
        public List<Car> cars;
        public RaceSession raceSession;
    }

    public class GameInit
    {
        public Race race;
    }

    public class Result
    {
        public int laps;
        public int ticks;
        public int millis;
    }

    public class Results
    {
        public Car car;
        public Result result;
    }

    public class BestLapResult
    {
        public int lap;
        public int ticks;
        public int millis;
    }

    public class BestLap
    {
        public Car car;
        public BestLapResult result;
    }

    public class GameEnd
    {
        public List<Results> results;
        public List<BestLap> bestLaps;
    }

    public class BotId
    {
        public string name;
        public string key;
    }

    public class Data
    {
        public BotId botId;
        public string trackName;
        public string password;
        public int carCount;
    }

    public class JoinRace : SendMsg
    {
        public Data data;

        public JoinRace(string botName, string botKey, string trackName, string password, int carCount) 
            : base(0)
        {
            data = new Data();
            data.botId = new BotId();
            data.botId.name = botName;
            data.botId.key = botKey;
            data.trackName = trackName;
            data.password = password;
            data.carCount = carCount;
        }

        protected override object MsgData()
        {
            return data;
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }

    public class CreateRace : SendMsg
    {

        public Data data;

        public CreateRace(string botName, string botKey, string trackName, string password, int carCount)
            : base(0)
        {
            data = new Data();
            data.botId = new BotId();
            data.botId.name = botName;
            data.botId.key = botKey;
            data.trackName = trackName;
            data.password = password;
            data.carCount = carCount;
        }

        protected override string MsgType()
        {
            return "createRace";
        }
    }

    public class Turbo : ReceiveMsg
    {
        public double turboDurationMilliseconds;

        public int turboDurationTicks;

        public double turboFactor;

        internal int TickStarted = -1;

        internal bool Ready;

        internal bool IsValid()
        {
            return Ready && TickStarted <= -1;
        }

        internal bool UsingNow(int tick)
        {
            return TickStarted >= 0;
        }

        internal double TurboFactor(int tick)
        {
            return UsingNow(tick) ? turboFactor : 1;
        }
    }

    public class UseTurbo : SendMsg
    {
        private readonly string msg;

        public UseTurbo(string msg, int gameTick) : base(gameTick)
        {
            this.msg = msg;
        }

        protected override object MsgData()
        {
            return msg;
        }

        protected override string MsgType()
        {
            return "turbo";
        }
    }

    public class TurboNotification : ReceiveMsg
    {
        public string name;
        public string color;
    }

    public class LapTime
    {
        public int lap;
        public int ticks;
        public int millis;
    }

    public class RaceTime
    {
        public int laps;
        public int ticks;
        public int millis;
    }

    public class Ranking
    {
        public int overall;
        public int fastestLap;
    }

    public class LapFinished : ReceiveMsg
    {
        public CarId car;
        public LapTime lapTime;
        public RaceTime raceTime;
        public Ranking ranking;
    }

    public class Crash : ReceiveMsg
    {
        public string name;
        public string color;
    }

    public class Spawn : ReceiveMsg
    {
        public string name;
        public string color;
    }

    public class Dnf
    {
        public CarId car;
        public string reason;
    }
}