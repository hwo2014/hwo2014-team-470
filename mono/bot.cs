﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using HelloWorldOpen.Engine;
using HelloWorldOpen.Protocol;

namespace HelloWorldOpen
{
    public class Bot
    {
        /*
        private static string _botKey;
        private static string _host;
        private static int _port;
        private static string _pass;
        private static int _cars;
        private static int _trackIndex = 1;
        
        public static void Main(string[] args)
        {
            if (args == null || args.Length < 4)
            {
                Console.WriteLine("Invalid command line parameters");
                Console.WriteLine("Example command line parameters:");
                Console.WriteLine("testserver.helloworldopen.com 8091 botName botKey");
                return;
            }

            _host = args[0];
            _port = int.Parse(args[1]);
            //string botName = args[2];
            _botKey = args[3];
            
            while (true)
            {
                _trackIndex = Helpers.NextOrFirst(_trackIndex, 4);
                try
                {
                    _pass = Guid.NewGuid().ToString();
                    Console.Write("Track name: 0 = keim, 1 = ger, 2 = usa, 3 = fra, 4 = eng, 5 = ita, 6 = jpn :: ");
                    _trackIndex = int.Parse(Console.ReadLine());
                    Console.Write("Cars :: ");
                    _cars = int.Parse(Console.ReadLine());
                    //_cars = new Random().Next(2, 3);
                    AutoResetEvent[] events = new AutoResetEvent[_cars];
                    for (int i = 0; i < _cars; ++i)
                    {
                        AutoResetEvent e = new AutoResetEvent(false);
                        events[i] = e;
                        Thread t = new Thread(Run);
                        t.IsBackground = true;
                        t.Start(e);
                        e.WaitOne();
                    }

                    WaitHandle.WaitAll(events);
                }
                catch (FormatException)
                {
                    break;
                }
                catch (Exception ex)
                {
                    Log.Debug(ex.GetType().FullName + ": " + ex);
                    break;
                }
            }

            Log.Debug("End...");
        }
        private static void Run(object state)
        {
            AutoResetEvent e = (AutoResetEvent) state;
            try
            {
                string trackName;
                switch (_trackIndex)
                {
                    case 0:
                        trackName = "keimola";
                        break;
                    case 1:
                        trackName = "germany";
                        break;
                    case 2:
                        trackName = "usa";
                        break;
                    case 3:
                        trackName = "france";
                        break;
                    case 4:
                        trackName = "england";
                        break;
                    case 5:
                        trackName = "imola";
                        break;
                    case 6:
                        trackName = "suzuka";
                        break;
                    default:
                        throw new Exception();
                }

                string name = "Astro-" + Thread.CurrentThread.ManagedThreadId;
                SendMsg joinModeMessage = new JoinRace(name, _botKey, trackName, _pass, _cars);
                Log.Debug("Connecting to " + _host + ":" + _port + " as " + name + "/" + _botKey);
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect(_host, _port);
                using (NetworkStream stream = new NetworkStream(socket, true))
                {
                    GameEngine bot = new GameEngine(stream);
                    bot.Join(joinModeMessage);
                    e.Set();
                    bot.Tournament();
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.ToString());
            }
            finally
            {
                e.Set();
            }
        }
        */
        public static void Main(string[] args) 
        {
            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];

            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using(TcpClient client = new TcpClient(host, port)) 
            {
                NetworkStream stream = client.GetStream();
                GameEngine bot = new GameEngine(stream);
                bot.Join(new Join(botName, botKey, 0));
                bot.Tournament();
            }
        }

    }
}

