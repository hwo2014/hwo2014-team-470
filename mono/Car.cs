﻿using System;
using System.Collections.Generic;
using HelloWorldOpen.Protocol;

namespace HelloWorldOpen.Engine
{
    public class RaceCar
    {
        private Queue<Turbo> _turbos = new Queue<Turbo>();

        public Turbo Turbo = new Turbo();

        public CarProperties Properties;

        public double Throttle = 1;

        public double Angle = 0;

        public LaneDetail CurrentLane;

        public Piece NextSwitchPiece = new Piece();

        public int CurrentGameTick = 0;

        public double CurrentInPiecePosition = 0;

        public bool IsSwitching = false;

        public double Acceleration = 0;

        public double MaxBreakForce;

        public double Speed;

        public double _distanceSinceLastUpdate;

        public double MaxAdjust;

        public Piece CurrentTrackPiece = new Piece();

        /// <summary>
        /// Initializes a new instance of the <see cref="RaceCar" /> class.
        /// </summary>
        /// <param name="car">The car.</param>
        public RaceCar(Car car)
        {
            Properties = new CarProperties(car.id.name, car.id.color, car.dimensions.width, car.dimensions.length, car.dimensions.guideFlagPosition);
        }

        public bool IsTurboMode
        {
            get { return Turbo.UsingNow(CurrentGameTick); }
        }

        public bool IsTurboAvailable
        {
            get { return Turbo.IsValid(); }
        }

        /// <summary>
        /// Updates the car positin.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="track">The track.</param>
        /// <param name="gameTick">The game tick.</param>
        public void Update(Position position, Track track, int gameTick)
        {
            Angle = position.angle;
            _distanceSinceLastUpdate = CalculateDistance(position, track);
            double lastInPiecePosition = CurrentInPiecePosition;
            CurrentInPiecePosition = position.piecePosition.inPieceDistance;
            
            CurrentTrackPiece = track.pieces[position.piecePosition.pieceIndex];
            CurrentLane = track.lanes[position.piecePosition.lane.endLaneIndex];

            
            if (position.piecePosition.IsSwitching() || (!IsSwitching && !position.piecePosition.IsSwitching()))
            {
                double lastSpeed = Speed;
                double speed = _distanceSinceLastUpdate;

                Acceleration = (Math.Pow(speed, 2) - Math.Pow(lastSpeed, 2)) / speed;
                Speed = speed;

                if (gameTick < 10 && Acceleration > 0 && !IsTurboMode)
                {
                    if (Acceleration > MaxBreakForce)
                    {
                        
                        MaxAdjust = Math.Max(0, 0.39 - Acceleration);
                        MaxBreakForce = Acceleration; // TODO
                        Log.Debug(gameTick,
                            "ACCEL C: " + CurrentTrackPiece.Index + ", " + _distanceSinceLastUpdate +
                            " (" + lastInPiecePosition + " / " + CurrentInPiecePosition + "), " + lastSpeed + " --> " + speed +
                            " (" + (speed - lastSpeed) + "), " + Acceleration);
                    }
                }
            }

            CurrentGameTick = gameTick;
            IsSwitching = position.piecePosition.IsSwitching();
            Properties.CurrentLap = position.piecePosition.lap;
        }

        private double CalculateDistance(Position position, Track track)
        {
            if (CurrentLane == null)
            {
                return 0;
            }
            // While swithing a peace, distance is a bit longer but it does not matter, still keeps me on track
            int currentIndex = CurrentTrackPiece == null ? 0 : CurrentTrackPiece.Index;
            LaneDetail currentLane = CurrentLane;
            if (currentIndex == position.piecePosition.pieceIndex)
            {
                return position.piecePosition.inPieceDistance - CurrentInPiecePosition;
            }
            Piece previousPiece = track.pieces[currentIndex];
            
            double distance = previousPiece.GetRemainingLength(CurrentInPiecePosition, currentLane.distanceFromCenter);
            int index = position.piecePosition.pieceIndex;
            while (true)
            {
                if (index == position.piecePosition.pieceIndex)
                {
                    distance += position.piecePosition.inPieceDistance;
                    return distance;
                }
                double pieceLen = track.pieces[index].GetLength(currentLane.distanceFromCenter);
                distance += pieceLen;
                index = Helpers.NextOrFirst(index, track.pieces.Count);
            }
        }

        public double GetAngle()
        {
            return Math.Abs(Angle);
        }

        public double TurboFactor()
        {
            return Turbo.TurboFactor(CurrentGameTick);
        }
        
        public void UseTurbo()
        {
            Turbo.TickStarted = CurrentGameTick;
        }

        public double Friction()
        {
            return -MaxBreakForce;
        }

        public bool CanSwitchLane(RaceCar opponent, int targetDistanceFromCenter)
        {
            if (opponent.CurrentLane.distanceFromCenter != targetDistanceFromCenter)
            {
                return true;
            }
            if (opponent.CurrentTrackPiece.Index != CurrentTrackPiece.Index)
            {
                return true; // TODO: some logic here...
            }

            if (opponent.CurrentInPiecePosition < CurrentInPiecePosition)
            {
                return true;
            }

            // TODO: some logic here...
            return false;
        }

        public bool IsHeadOf(RaceCar myCar, List<PieceGroup> groups)
        {
            PieceGroup myCarGroup = groups[myCar.CurrentTrackPiece.PieceGroupIndex];
            PieceGroup opponentGroup = groups[CurrentTrackPiece.PieceGroupIndex];
            
            if ((myCar.CurrentLane.distanceFromCenter == CurrentLane.distanceFromCenter || 
                 myCar.NextSwitchPiece.PreferredEndLane.distanceFromCenter == CurrentLane.distanceFromCenter) 

                &&

               (CurrentTrackPiece.PieceGroupIndex == Helpers.NextOrFirst(myCar.CurrentTrackPiece.PieceGroupIndex, groups.Count) 
                 || 
               (myCarGroup.GroupId == opponentGroup.GroupId 
                    && 
                myCarGroup.GetPositionInGroup(myCar.CurrentTrackPiece.Index, myCar.CurrentInPiecePosition, myCar.CurrentLane.distanceFromCenter) 
                 <
                opponentGroup.GetPositionInGroup(CurrentTrackPiece.Index, CurrentInPiecePosition, CurrentLane.distanceFromCenter))))
            {
                return true;
            }
            return false;
        }

        public void TurboAvailable(Turbo turbo)
        {
            if (_turbos.Count == 0)
            {
                turbo.Ready = true;
                if (IsTurboMode)
                {
                    _turbos.Enqueue(turbo);
                    Log.Debug("TURBO QUEUED, count " + _turbos.Count);
                }
                else
                {
                    Turbo = turbo;
                    Log.Debug("TURBO AVAILABLE: " + IsTurboAvailable + ", count " + _turbos.Count);
                }
            }
            else
            {
                Log.Debug("TURBO ALREADY QUEUED");
            }
        }

        public void TurboEnded()
        {
            if (_turbos.Count > 0)
            {
                Turbo = _turbos.Dequeue();
                Log.Debug("TURBO DEQUEUED, TURBO AVAILABLE: " + IsTurboAvailable);
            }
            else
            {
                Turbo = new Turbo();
            }
        }

        public void ClearTurbo()
        {
            Turbo = new Turbo();
            if (_turbos.Count > 0)
            {
                _turbos.Dequeue();
            }       
        }
    }

    public class CarProperties
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CarProperties"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="color">The color.</param>
        /// <param name="width">The width.</param>
        /// <param name="length">The length.</param>
        /// <param name="guideFlagPosition">The guide flag position.</param>
        public CarProperties(string name, string color, double width, double length, double guideFlagPosition)
        {
            Name = name;
            Color = color;
            Width = width;
            Length = length;
            GuidanceFlagPosition = guideFlagPosition;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name;

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        public string Color;

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        public double Width;

        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        public double Length;

        /// <summary>
        /// Gets or sets the guidance flag position.
        /// </summary>
        public double GuidanceFlagPosition;

        public int CurrentLap = 1;
    }
}
