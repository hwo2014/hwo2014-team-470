﻿using System;
using System.Collections.Generic;
using HelloWorldOpen.Protocol;

namespace HelloWorldOpen.Engine
{
    public static class Helpers
    {

        /// <summary>
        /// Gets the <see cref="Position"/> of the <paramref name="car"/>.
        /// </summary>
        /// <param name="positions">The positions.</param>
        /// <param name="car">The car.</param>
        /// <returns>The <see cref="Position"/> of the <paramref name="car"/>.</returns>
        /// <exception cref="System.InvalidOperationException">Thrown when sequence does not contain any items</exception>
        public static Position GetPosition(IEnumerable<Position> positions, RaceCar car)
        {
            foreach (Position p in positions)
            {
                if (p.id.name == car.Properties.Name)
                {
                    return p;
                }
            }
            throw new InvalidOperationException("Sequence does not contain any items ;) ");
        }

        /// <summary>
        /// Gets my car from the list of race cars.
        /// </summary>
        /// <param name="myId">My identifier.</param>
        /// <param name="cars">The cars.</param>
        /// <returns>My car</returns>
        /// <exception cref="System.InvalidOperationException">Sequence does not contain any items ;) </exception>
        public static RaceCar GetMyCar(IEnumerable<RaceCar> cars, CarId myId)
        {
            foreach (RaceCar car in cars)
            {
                if (car.Properties.Name == myId.name)
                {
                    return car;
                }
            }
            throw new InvalidOperationException("Sequence does not contain any items ;) ");
        }

        /// <summary>
        /// Gets the race cars.
        /// </summary>
        /// <param name="cars">The cars.</param>
        /// <returns></returns>
        public static IList<RaceCar> GetRaceCars(IEnumerable<Car> cars)
        {
            List<RaceCar> retval = new List<RaceCar>();

            foreach (Car car in cars)
            {
                retval.Add(new RaceCar(car));
            }

            return retval;
        }

        /// <summary>
        /// Returns the next index from the <paramref name="current"/> or zero if the <paramref name="current"/> plus one (1) is equal or greater than the <paramref name="limit"/>.
        /// </summary>
        /// <param name="current">The current.</param>
        /// <param name="limit">The limit.</param>
        /// <returns></returns>
        public static int NextOrFirst(int current, int limit)
        {
            return current + 1 >= limit ? 0 : current + 1;
        }

        /// <summary>
        /// Returns the previous index from before the <paramref name="current"/> or <paramref name="limit"/> - 1 if the <paramref name="current"/> minus one (1) is less than zero (0).
        /// </summary>
        /// <param name="current">The current.</param>
        /// <param name="limit">The limit.</param>
        /// <returns></returns>
        public static int PreviousOrLast(int current, int limit)
        {
            return current - 1 < 0 ? limit - 1 : current - 1;
        }
    }
}