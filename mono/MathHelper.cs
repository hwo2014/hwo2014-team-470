﻿using System;
using HelloWorldOpen.Protocol;

namespace HelloWorldOpen.Mathematics
{
    /// <summary>
    /// Class containing math related helper methods.
    /// </summary>
    public static class MathHelper
    {

        private const double DegToRadConstant = Math.PI / 180.0;

        /// <summary>
        /// Calculates the length of the bend.
        /// </summary>
        /// <param name="radius">The radius.</param>
        /// <param name="angle">The angle.</param>
        /// <param name="distanceFromCenter">The distance from center.</param>
        /// <returns>The length of the arc.</returns>
        public static double CalculateArcLength(double radius, double angle, double distanceFromCenter)
        {
            double radians = DegToRad(angle);
            return radians * (radius + distanceFromCenter);
        }

        public static double CalculateBrakingPoint(double currentSpeed, double targetSpeed, double acceleration)
        {
            if (currentSpeed <= targetSpeed)
            {
                return 0;
            }
            // TODO: think
            double time = Math.Abs((currentSpeed - targetSpeed) / acceleration);
            double distance = (currentSpeed * time) + (0.5 * acceleration * Math.Pow(time, 2));
            return distance;
        }

        public static double GetAngularAcceleration(double radius, double angle, double acceleration)
        {
            return Math.Sqrt(Math.Abs(acceleration * radius));
        }

        /// <summary>
        /// Converts from degrees to radians.
        /// </summary>
        /// <param name="angle">The angle.</param>
        /// <returns></returns>
        public static double DegToRad(double angle)
        {
            return Math.Abs(angle) * DegToRadConstant;
        }
    }
}
